from django.contrib import admin
from backend.models import Label, Store


@admin.register(Label)
class LabelAdmin(admin.ModelAdmin):
    pass


@admin.register(Store)
class StoreAdmin(admin.ModelAdmin):
    list_display = ('name', 'location', 'opening_hour', 'closing_hour',
                    'has_delivery', 'active')
