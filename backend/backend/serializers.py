# from rest_framework import serializers
from backend.models import Store
from rest_framework_gis import serializers


class StoreSerializer(serializers.GeoFeatureModelSerializer):
    class Meta:
        model = Store
        geo_field = 'location'
        fields = ('id', 'name', 'address', 'location', 'contact_name',
                  'phone_number', 'email', 'url', 'opening_hour',
                  'closing_hour', 'has_delivery')
