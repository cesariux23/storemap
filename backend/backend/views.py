from backend.serializers import StoreSerializer
from backend.models import Store
from rest_framework import generics


class StoreList(generics.ListCreateAPIView):
    """Handles get and post methods.

    If method is 'get' all stores are returned, if method is 'post'
    a new store is added.
    """
    queryset = Store.objects.all()
    serializer_class = StoreSerializer


class StoreDetail(generics.RetrieveAPIView):
    """Get a specific Store by id.
    """
    queryset = Store.objects.all()
    serializer_class = StoreSerializer
