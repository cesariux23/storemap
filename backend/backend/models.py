"""Define backend's models.
"""
from django.contrib.gis.db import models


class Label(models.Model):
    """This models store the labels assigned to a store.

    This labels can be categories, types of food, etc.
    """
    name = models.CharField(max_length=50, unique=True)
    description = models.CharField(max_length=800,
                                   blank=True,
                                   null=True,
                                   default=None)

    def __str__(self):
        return self.name


class Store(models.Model):
    """Keep track of stores info, like location and contact name.
    """
    name = models.CharField(max_length=128,
                            blank=True,
                            null=True,
                            default=None)
    address = models.CharField(max_length=128,
                               blank=True,
                               null=True,
                               default=None)
    location = models.PointField(blank=True, null=True, default=None)
    contact_name = models.CharField(max_length=64,
                                    blank=True,
                                    null=True,
                                    default=None)
    phone_number = models.CharField(max_length=12,
                                    blank=True,
                                    null=True,
                                    default=None)
    email = models.EmailField(blank=True, null=True, default=None)
    url = models.URLField(blank=True, null=True, default=None)
    opening_hour = models.TimeField(blank=True, null=True, default=None)
    closing_hour = models.TimeField(blank=True, null=True, default=None)
    has_delivery = models.BooleanField(default=False)
    active = models.BooleanField(default=True)
    label = models.ManyToManyField(Label)

    def __str__(self):
        return f'{self.name} - {self.location}'
