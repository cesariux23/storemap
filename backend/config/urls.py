from django.urls import path
from django.contrib import admin
from django.contrib.auth import logout
from backend.views import StoreDetail, StoreList
from django.conf.urls import include

from config.api import api

urlpatterns = [
    path('admin/', admin.site.urls, name='admin'),
    path('logout/', logout, {'next_page': '/'}, name='logout'),
    path('api/store/', StoreList.as_view(), name='store_list'),
    path('api/store/<int:pk>/', StoreDetail.as_view(), name='store_detail'),
    path('api/', include(api.urls)),
    path('api-auth/', include('rest_framework.urls',
                              namespace='rest_framework')),
]
